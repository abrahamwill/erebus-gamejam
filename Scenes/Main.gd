extends Node2D

var timeValue = 200
onready var time_label = get_node("time_label")
onready var game_timer = get_node("game_timer")

func _ready():
	game_timer.set_wait_time(1)
	game_timer.start()

func _on_game_timer_timeout():
	timeValue -= 1
	if timeValue == 0:
		get_tree().exit()
	_ready()