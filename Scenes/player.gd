extends KinematicBody2D

onready var cam = $CameraAnchor

var gravity_velo = Vector2()
var player_velo = Vector2()
var grapple_pos = Vector2()
#Grapple state
#0 = released
#1 = firing grapple
#2 = grapple connected
var grapple_state = 0

const gravity = -600
const player_gravity = -300
const grapple_speed = 6000
const pull_acceleration = 4000
const DRAG = 1

func _ready():
	grapple_pos = global_position

#draw grapple
func _draw():
	var l_pos = grapple_pos - global_position
	draw_circle(l_pos, 5 , Color(255, 106, 240))
	draw_line(Vector2(), l_pos, Color(255, 106, 240))

func _process(delta):
	if Input.is_action_pressed("exit"):
		get_tree().quit()
	if Input.is_action_just_pressed("fire"):
		fire()
	elif Input.is_action_just_pressed("release"):
		release()
		
	var mouse_position = get_global_mouse_position()
	var player_position = global_position
	player_position += (mouse_position - player_position) / 2
	cam.global_position = player_position
	
	


func _physics_process(delta):
	process_grapple(delta)
	
	if is_on_floor() and grapple_state != 2:
		player_velo.y = 0
	else:
		player_velo.y -= player_gravity * delta

	if grapple_state == 2:
		var climb_force = grapple_pos - global_position
		climb_force = climb_force.normalized()
		climb_force *= pull_acceleration
		player_velo += delta * climb_force
	
	if grapple_state == 0:
		grapple_pos = global_position
	
	player_velo -= DRAG * player_velo * delta
	
	move_and_slide(player_velo, Vector2(0, -1))
	if get_slide_count() > 0 and get_slide_collision(0).collider.name == "SpikeMap":
		get_tree().reload_current_scene()
	update()


func process_grapple(var delta):
	if grapple_state != 1:
		return
	
	gravity_velo.y -= gravity * delta
	var last_pos = grapple_pos
	grapple_pos += gravity_velo * delta
	
	var space_state = get_world_2d().direct_space_state
	var result = space_state.intersect_ray(last_pos, grapple_pos, [self])
	
	if result:
		grapple_pos = result.position
		grapple_state = 2
		gravity_velo = 0

func fire():
	release()
	grapple_state = 1
	var m_pos = get_local_mouse_position()
	m_pos = m_pos.normalized()
	gravity_velo = m_pos * grapple_speed

func release():
	grapple_state = 0
	grapple_pos = global_position
	update()
